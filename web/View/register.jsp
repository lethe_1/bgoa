<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="viewport"
              content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <title>BGOA - 注册页面</title>

        <!-- Set render engine for 360 browser -->
        <meta name="renderer" content="webkit">

        <!-- No Baidu Siteapp-->
        <meta http-equiv="Cache-Control" content="no-siteapp"/>

        <link rel="icon" type="image/png" href="assets/i/favicon.png">

        <!-- Add to homescreen for Chrome on Android -->
        <meta name="mobile-web-app-capable" content="yes">
        <link rel="icon" sizes="192x192" href="assets/i/app-icon72x72@2x.png">

        <!-- Add to homescreen for Safari on iOS -->
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta name="apple-mobile-web-app-title" content="Amaze UI"/>
        <link rel="apple-touch-icon-precomposed" href="assets/i/app-icon72x72@2x.png">

        <!-- Tile icon for Win8 (144x144 + tile color) -->
        <meta name="msapplication-TileImage" content="assets/i/app-icon72x72@2x.png">
        <meta name="msapplication-TileColor" content="#0e90d2">

        <link rel="stylesheet" href="assets/css/amazeui.min.css">
        <link rel="stylesheet" href="assets/css/app.css">

        <style>
            .header {
              text-align: center;
            }
            .header h1 {
              font-size: 200%;
              color: #333;
              margin-top: 30px;
            }
            .header p {
              font-size: 14px;
            }
        </style>

    </head>
    <body>
        <div class="header">
            <div class="am-g">
                <h1>BGOA 服务端功能测试</h1>
                <p>Integrated Development Environment</p>
            </div>
            <hr />
        </div>
        <div class="am-g">
            <div class="am-u-lg-6 am-u-md-8 am-u-sm-centered">
                <h3>注册</h3>
                <hr>        
                <br>
                <form id="registerForm" method="post" class="am-form">
                    <label for="username">用户名:</label>
                    <input type="text" name="username" id="username" value="" required>
                    <br>
                    <label for="password">密码:</label>
                    <input type="password" name="password" id="password" value="">
                    <br>
                    <label for="telephone">联系电话:</label>
                    <input type="text" name="telephone" id="telephone" value="">
                    <br>
                    <label for="idnumber">身份证号:</label>
                    <input type="text" name="idnumber" id="idnumber" value="">
                    <br>
                    <label for="dept">部门:</label>
                    <select name="dept" id="dept">
                        <c:forEach var='d' items='${obj.dept}'>
                            <option value="${d.getInt("dept_id")}">${d.getString("department")}</option>
                        </c:forEach>
                    </select>
                    <br>
                    <label for="role">角色:</label>
                    <select name="role" id="role">
                        <c:forEach var='r' items='${obj.role}'>
                            <option value="${r.getInt("role_id")}">${r.getString("role_name")}</option>
                        </c:forEach>
                    </select>
                    <br><br>
                    <div class="am-cf">
                        <input type="button" onclick="ajaxsubmit();" value="注册" class="am-btn am-btn-primary am-btn-sm am-fl">
                    </div>
                </form>
                <hr>
                <span id="test"></span>
                <p style="text-align: center;">© 2015 Lethe &copy; Interaction, Inc. </p>
            </div>
        </div>
        <div class="am-modal am-modal-alert" tabindex="-1" id="result-alert">
            <div class="am-modal-dialog">
                <div class="am-modal-hd" id="result"></div>
                <div class="am-modal-bd" id="resultText">
                    
                </div>
                <div class="am-modal-footer">
                    <span class="am-modal-btn">确定</span>
                </div>
            </div>
        </div>
        <!--[if (gte IE 9)|!(IE)]><!-->
        <script src="assets/js/jquery.min.js"></script>
        <!--<![endif]-->
        <!--[if lte IE 8 ]>
        <script src="http://libs.baidu.com/jquery/1.11.3/jquery.min.js"></script>
        <script src="http://cdn.staticfile.org/modernizr/2.8.3/modernizr.js"></script>
        <script src="assets/js/amazeui.ie8polyfill.min.js"></script>
        <![endif]-->
        <script src="assets/js/amazeui.min.js"></script>
        
        <script type="text/javascript">
            function ajaxsubmit(){
                var data=$("#registerForm").serialize();
                $.post("./do_register",data,function(rtext){
                    $("#test").html(rtext);
                    var r=JSON.parse(rtext);
                    if(r.result)
                    {
                        $("#result").html("注册成功！");
                        $("#resultText").html(rtext);
                    }
                    else
                    {
                        $("#result").html("注册失败！");
                        $("#resultText").html("服务器错误！"+r.reason);
                    }
                    $('#result-alert').modal();
                });
                return false;
            }
        </script>
    </body>
</html>

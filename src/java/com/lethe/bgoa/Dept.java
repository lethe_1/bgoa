package com.lethe.bgoa;

import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.Id;
import org.nutz.dao.entity.annotation.Table;

@Table("dept")
public class Dept {
 @Id
 private int dept_id;
 @Column
 private String department;
public Dept(int dept_id, String department) {
	super();
	this.dept_id = dept_id;
	this.department = department;
}
public Dept() {
	super();
}
public int getDept_id() {
	return dept_id;
}
public void setDept_id(int dept_id) {
	this.dept_id = dept_id;
}
public String getDepartment() {
	return department;
}
public void setDepartment(String department) {
	this.department = department;
}
@Override
public String toString() {
	return "Dept [dept_id=" + dept_id + ", department=" + department + "]";
}
 
}

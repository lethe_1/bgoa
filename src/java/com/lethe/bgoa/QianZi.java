package com.lethe.bgoa;

import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.Id;
import org.nutz.dao.entity.annotation.Table;

@Table("qianzi")
public class QianZi {
    
    @Id
    private int qid;

    public int getQid() {
        return qid;
    }

    public void setQid(int qid) {
        this.qid = qid;
    }
    
    @Column
    private String gongzuoling;

    public String getGongzuoling() {
        return gongzuoling;
    }

    public void setGongzuoling(String gongzuoling) {
        this.gongzuoling = gongzuoling;
    }
    
    @Column
    private String zhijian;
    @Column
    private String zhuguan;
    @Column
    private String jiaotu;
    @Column
    private String huitu;

    public QianZi(String gongzuoling,String zhijian, String zhuguan, String jiaotu, String huitu) {
        super();
        this.gongzuoling=gongzuoling;
        this.zhijian = zhijian;
        this.zhuguan = zhuguan;
        this.jiaotu = jiaotu;
        this.huitu = huitu;
    }

    public QianZi() {
        super();
    }

    public String getZhijian() {
        return zhijian;
    }

    public void setZhijian(String zhijian) {
        this.zhijian = zhijian;
    }

    public String getZhuguan() {
        return zhuguan;
    }

    public void setZhuguan(String zhuguan) {
        this.zhuguan = zhuguan;
    }

    public String getJiaotu() {
        return jiaotu;
    }

    public void setJiaotu(String jiaotu) {
        this.jiaotu = jiaotu;
    }

    public String getHuitu() {
        return huitu;
    }

    public void setHuitu(String huitu) {
        this.huitu = huitu;
    }

    @Override
    public String toString() {
        return "QianZi [zhijian=" + zhijian + ", zhuguan=" + zhuguan + ", jiaotu="
                + jiaotu + ", huitu=" + huitu + "]";
    }
}

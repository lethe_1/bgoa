package com.lethe.bgoa;

import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.Id;
import org.nutz.dao.entity.annotation.Table;

@Table("user")
public class User {
    @Id
    private int user_id;
    @Column
    private int dept_id;
    @Column
    private int role_id;
    @Column
    private String username;
    @Column
    private String password;
    @Column
    private String telephone;
    @Column
    private String number;

    public User(int dept_id, int role_id, String userName,
            String password, String userTelephone, String certifNumber) {
        this.dept_id = dept_id;
        this.role_id = role_id;
        this.username = userName;
        this.password = password;
        this.telephone = userTelephone;
        this.number = certifNumber;
    }

    public User() {
        super();
    }

    public int getUserId() {
        return user_id;
    }

    public void setUserId(int userId) {
        this.user_id = userId;
    }

    public int getDept_id() {
        return dept_id;
    }

    public void setDept_id(int dept_id) {
        this.dept_id = dept_id;
    }

    public int getRole_id() {
        return role_id;
    }

    public void setRole_id(int role_id) {
        this.role_id = role_id;
    }

    public String getUserName() {
        return username;
    }

    public void setUserName(String userName) {
        this.username = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserTelephone() {
        return telephone;
    }

    public void setUserTelephone(String userTelephone) {
        this.telephone = userTelephone;
    }

    public String getCertifNumber() {
        return number;
    }

    public void setCertifNumber(String certifNumber) {
        number = certifNumber;
    }

    @Override
    public String toString() {
        return "User [userId=" + user_id + ", dept_id=" + dept_id + ", role_id="
                + role_id + ", userName=" + username + ", password=" + password
                + ", userTelephone=" + telephone + ", CertifNumber="
                + number + "]";
    }
}

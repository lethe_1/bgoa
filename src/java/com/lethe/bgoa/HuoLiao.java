package com.lethe.bgoa;

import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.Name;
import org.nutz.dao.entity.annotation.Table;

@Table("huoliao")
public class HuoLiao {
    @Column
    private String tuhao;
    @Name
    private String gongzuoling;
    @Column
    private String hl_date;
    @Column
    private int shuliang;
    @Column
    private String beiliao;
    @Column
    private String mingcheng;
    @Column
    private boolean gongzuo;
    @Column
    private boolean kufang;

    public boolean isGongzuo() {
        return gongzuo;
    }

    public void setGongzuo(boolean gongzuo) {
        this.gongzuo = gongzuo;
    }

    public boolean isKufang() {
        return kufang;
    }

    public void setKufang(boolean kufang) {
        this.kufang = kufang;
    }

    public HuoLiao(String tuhao, String gongzuoling, String data, int shuliang,
            String beiliao, String mingcheng) {
        super();
        this.tuhao = tuhao;
        this.gongzuoling = gongzuoling;
        this.hl_date = data;
        this.shuliang = shuliang;
        this.beiliao = beiliao;
        this.mingcheng = mingcheng;
    }

    public HuoLiao() {
        super();
    }

    public String getTuhao() {
        return tuhao;
    }

    public void setTuhao(String tuhao) {
        this.tuhao = tuhao;
    }

    public String getGongzuoling() {
        return gongzuoling;
    }

    public void setGongzuoling(String gongzuoling) {
        this.gongzuoling = gongzuoling;
    }

    public String getDate() {
        return hl_date;
    }

    public void setDate(String date) {
        this.hl_date = date;
    }

    public int getShuliang() {
        return shuliang;
    }

    public void setShuliang(int shuliang) {
        this.shuliang = shuliang;
    }

    public String getBeiliao() {
        return beiliao;
    }

    public void setBeiliao(String beiliao) {
        this.beiliao = beiliao;
    }

    public String getMingcheng() {
        return mingcheng;
    }

    public void setMingcheng(String mingcheng) {
        this.mingcheng = mingcheng;
    }
    
    

    @Override
    public String toString() {
        return "HuoLiao [tuhao=" + tuhao + ", gongzuoling=" + gongzuoling
                + ", data=" + hl_date + ", shuliang=" + shuliang + ", beiliao="
                + beiliao + ", mingcheng=" + mingcheng + "]";
    }
}

package com.lethe.bgoa;

import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.Name;
import org.nutz.dao.entity.annotation.Table;

@Table("kufang")
public class KuFang {
    @Name
    private String gongzuoling;
    @Column
    private int hegeshu;
    @Column
    private int rukushu;
    @Column
    private int fengcunshu;
    
    

    public KuFang(String gongzuoling, int hegeshu, int rukushu, int fengcunshu) {
        super();
        this.gongzuoling = gongzuoling;
        this.hegeshu = hegeshu;
        this.rukushu = rukushu;
        this.fengcunshu = fengcunshu;
    }

    public KuFang() {
        super();
    }

    public String getGongzuoling() {
        return gongzuoling;
    }

    public void setGongzuoling(String gongzuoling) {
        this.gongzuoling = gongzuoling;
    }

    public int getHegeshu() {
        return hegeshu;
    }

    public void setHegeshu(int hegeshu) {
        this.hegeshu = hegeshu;
    }

    public int getRukushu() {
        return rukushu;
    }

    public void setRukushu(int rukushu) {
        this.rukushu = rukushu;
    }

    public int getFengcunshu() {
        return fengcunshu;
    }

    public void setFengcunshu(int fengcunshu) {
        this.fengcunshu = fengcunshu;
    }

    @Override
    public String toString() {
        return "KuFang [gongzuoling=" + gongzuoling + ", hegeshu=" + hegeshu
                + ", rukushu=" + rukushu + ", fengcunshu=" + fengcunshu + "]";
    }
}

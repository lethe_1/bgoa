package com.lethe.bgoa;

import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.Id;
import org.nutz.dao.entity.annotation.Table;

@Table("role")
public class Role {
    @Id
    private int role_id;
    @Column
    private String role_name;

    public Role(int role_id, String role_name) {
        super();
        this.role_id = role_id;
        this.role_name = role_name;
    }

    public Role() {
        super();
    }

    public int getRole_id() {
        return role_id;
    }

    public void setRole_id(int role_id) {
        this.role_id = role_id;
    }

    public String getRole_name() {
        return role_name;
    }

    public void setRole_name(String role_name) {
        this.role_name = role_name;
    }

    @Override
    public String toString() {
        return "Role [role_id=" + role_id + ", role_name=" + role_name + "]";
    }
}

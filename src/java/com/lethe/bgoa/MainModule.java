package com.lethe.bgoa;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.nutz.dao.Chain;
import org.nutz.dao.Cnd;
import org.nutz.dao.Dao;
import org.nutz.dao.entity.Record;
import org.nutz.mvc.Mvcs;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.IocBy;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.Param;
import org.nutz.mvc.ioc.provider.JsonIocProvider;
import org.nutz.trans.Atom;

/**
 *
 * @author lethe
 */
@IocBy(type=JsonIocProvider.class,args={"config"})
public class MainModule {
    @At("/")
    @Ok("json")
    public Object doTest(){
        Dao dao=getDao();
        Record re=dao.fetch("users",Cnd.where("id", "=", 2));
        return re;
    }

    private Dao getDao() {
        return Mvcs.getIoc().get(Dao.class);
    }
    
    @At("/getDepts")
    @Ok("json")
    public List<Dept> getDepts(){
        List<Dept> depts=getDao().query(Dept.class, null);
        return depts;
    }
    @At("/getDept")
    @Ok("json")
    public Dept getDepts(int dept_id){
        Dept dept=getDao().fetch(Dept.class, dept_id);
        return dept;
    }
    
    @At("/getGongzuo")
    @Ok("json")
    public GongZuo getGongzuo(int gongxu_id){
        return getDao().fetch(GongZuo.class,gongxu_id);
    }
    
    @At("/getHuoliao")
    @Ok("json")
    public HuoLiao getHuoliao(String gongzuoling){
        return getDao().fetch(HuoLiao.class,gongzuoling);
    }
    
    @At("/getKufang")
    @Ok("json")
    public KuFang getKufang(String gongzuoling){
        return getDao().fetch(KuFang.class,gongzuoling);
    }
    
    @At("/getQianzi")
    @Ok("json")
    public QianZi getQianzi(int qid){
        return getDao().fetch(QianZi.class,qid);
    }
    
    @At("/login")
    @Ok("jsp:/View/login")
    public void loginJsp(){
    }
    
    @At("/register")
    @Ok("jsp:/View/register")
    public HashMap registerJsp(){
        List<Record> dept=getDao().query("dept",null);
        List<Record> role=getDao().query("role",null);
        
        HashMap result=new HashMap();
        result.put("dept", dept);
        result.put("role", role);
        
        return result;
    }
    
    @At("/do_register")
    @Ok("json")
    public HashMap doRegister(int dept,int role,String username,String password,String telephone,String idnumber){
        HashMap result=new HashMap();
        result.put("result", false);
        User user=new User(dept, role, username, password, telephone, idnumber);
        getDao().insert(user);
        result.put("result", true);
        return result;
    }
    
    @At("/do_login")
    @Ok("json")
    public User doLogin(@Param("username")String username,@Param("password")String password){
        return getDao().fetch(User.class, Cnd.where("username", "=", username).and("password","=",password));
    }
    
    
    @At("/new_lailiao")
    @Ok("json")
    public HuoLiao do_new_lailiao(String riqi,String tuhao,String gongzuoling,String mingcheng,int shuliang,String beiliao){
        HuoLiao huoliao = new HuoLiao(tuhao, gongzuoling, riqi, shuliang, beiliao, mingcheng);
        QianZi qianzi=new QianZi(gongzuoling, "", "", "", "");
        getDao().insert(huoliao);
        getDao().insert(qianzi);
        return huoliao;
    }
    
    @At("get_qianzi")
    @Ok("json")
    public String do_get_qianzi(String who){
        String result=null;
        QianZi qianzi=getDao().fetch(QianZi.class,Cnd.where(who,"=",""));
        if(qianzi!=null){
            result=qianzi.getGongzuoling();
        }
        return result;
    }
    
    @At("get_gongzuo")
    @Ok("json")
    public String do_get_gongzuo(){
        String result=null;
        HuoLiao hl=getDao().fetch(HuoLiao.class,Cnd.where("gongzuo","=",false));
        if(hl!=null){
            result=hl.getGongzuoling();
        }
        return result;
    }
    
    @At("get_kufang")
    @Ok("json")
    public String do_get_kufang(){
        String result=null;
        HuoLiao hl=getDao().fetch(HuoLiao.class,Cnd.where("kufang","=",false));
        if(hl!=null){
            result=hl.getGongzuoling();
        }
        return result;
    }
    
    @At("/new_qianzi")
    @Ok("json")
    public QianZi do_new_zjqz(String gongzuoling,String zhijian,String huitu,String jiaotu,String zhuguan){
        QianZi qianzi= getDao().fetch(QianZi.class, Cnd.where("gongzuoling","=",gongzuoling));
        if(qianzi==null){
            qianzi=new QianZi(gongzuoling, zhijian, zhuguan, jiaotu, huitu);
            getDao().insert(qianzi);
        }else
        {
            if(zhijian!=null){
                qianzi.setZhijian(zhijian);
                getDao().update("huoliao", Chain.make("gongzuo", true), Cnd.where("gongzuoling", "=", gongzuoling));
            }
            if(huitu!=null) {
                qianzi.setHuitu(huitu);
            }
            if(jiaotu!=null) qianzi.setJiaotu(jiaotu);
            if(zhuguan!=null) qianzi.setZhuguan(zhuguan);
            getDao().update(qianzi);
        }
        return qianzi;        
    }
    
    @At("/new_gongzuo")
    @Ok("json")
    public GongZuo do_new_gongzuo(String gongzuoling,int gongxuhao,String gongxumingcheng,Date shoujian,
        Date jiaoyan,int hegeshu,int cancipin,String who){
        GongZuo gongzuo=new GongZuo(gongzuoling, gongxuhao, gongxumingcheng, shoujian, jiaoyan, hegeshu, cancipin, who);
        getDao().insert("gongzuo", Chain.make("gongzuoling", gongzuoling).add("gongxuhao", gongxuhao).add("gongxumingcheng",gongxumingcheng)
                .add("shouriqi",shoujian).add("jiaoriqi",jiaoyan).add("hege",hegeshu).add("cancipin",cancipin).add("who",who));
        return gongzuo;
    }
    
    @At("/new_kufang")
    @Ok("json")
    public KuFang do_new_kufang(String gongzuoling,int hegeshu,int rukushu,int fengcunshu){
        KuFang kufang=new KuFang(gongzuoling, hegeshu, rukushu, fengcunshu);
        getDao().insert(kufang);
        getDao().update("huoliao", Chain.make("kufang", true), Cnd.where("gongzuoling", "=", gongzuoling));
        return kufang;
    }
}

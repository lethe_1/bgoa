package com.lethe.bgoa;

import java.util.Date;
import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.Id;
import org.nutz.dao.entity.annotation.Table;

@Table("gongzuo")
public class GongZuo {

    @Id
    private int gongxu_id;
    @Column
    private String gongzuoling;
    @Column
    private int gongxuhao;
    @Column
    private String gongxumingcheng;
    @Column
    private Date shouriqi;
    @Column
    private Date jiaoriqi;
    @Column
    private int hege;
    @Column
    private int cancipin;
    @Column
    private String who;

    public int getGongxu_id() {
        return gongxu_id;
    }

    public void setGongxu_id(int gongxu_id) {
        this.gongxu_id = gongxu_id;
    }

    public String getGongzuoling() {
        return gongzuoling;
    }

    public void setGongzuoling(String gongzuoling) {
        this.gongzuoling = gongzuoling;
    }

    public int getGongxuhao() {
        return gongxuhao;
    }

    public void setGongxuhao(int gongxuhao) {
        this.gongxuhao = gongxuhao;
    }

    public String getGongxumingcheng() {
        return gongxumingcheng;
    }

    public void setGongxumingcheng(String gongxumingcheng) {
        this.gongxumingcheng = gongxumingcheng;
    }

    public Date getShouriqi() {
        return shouriqi;
    }

    public void setShouriqi(Date shouriqi) {
        this.shouriqi = shouriqi;
    }

    public Date getJiaoriqi() {
        return jiaoriqi;
    }

    public void setJiaoriqi(Date jiaoriqi) {
        this.jiaoriqi = jiaoriqi;
    }

    public int getHege() {
        return hege;
    }

    public void setHege(int hege) {
        this.hege = hege;
    }

    public int getCancipin() {
        return cancipin;
    }

    public void setCancipin(int cancipin) {
        this.cancipin = cancipin;
    }

    public String getWho() {
        return who;
    }

    public void setWho(String who) {
        this.who = who;
    }

    public GongZuo(String gongzuoling, int gongxuhao, String gongxumingcheng, Date shouriqi, Date jiaoriqi, int hege, int cancipin, String who) {
        this.gongzuoling = gongzuoling;
        this.gongxuhao = gongxuhao;
        this.gongxumingcheng = gongxumingcheng;
        this.shouriqi = shouriqi;
        this.jiaoriqi = jiaoriqi;
        this.hege = hege;
        this.cancipin = cancipin;
        this.who = who;
    }
    
    
}

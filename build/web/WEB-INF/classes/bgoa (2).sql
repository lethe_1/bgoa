-- phpMyAdmin SQL Dump
-- version phpStudy 2014
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2015 年 07 月 15 日 22:41
-- 服务器版本: 5.5.40
-- PHP 版本: 5.3.29

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `bgoa`
--

-- --------------------------------------------------------

--
-- 表的结构 `dept`
--

CREATE TABLE IF NOT EXISTS `dept` (
  `dept_id` int(11) NOT NULL AUTO_INCREMENT,
  `department` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`dept_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=13 ;

--
-- 转存表中的数据 `dept`
--

INSERT INTO `dept` (`dept_id`, `department`) VALUES
(9, '总经理'),
(2, '技术室'),
(3, '库房'),
(4, '钳焊组'),
(5, '激切组'),
(10, '副总经理'),
(7, '产品组'),
(8, '销售部'),
(11, '生产主管'),
(12, '副主管');

-- --------------------------------------------------------

--
-- 表的结构 `gongzuo`
--

CREATE TABLE IF NOT EXISTS `gongzuo` (
  `gongxu_id` int(16) NOT NULL AUTO_INCREMENT,
  `gongzuoling` varchar(255) COLLATE utf8_bin NOT NULL,
  `gongxumingcheng` varchar(255) COLLATE utf8_bin NOT NULL,
  `shouriqi` date NOT NULL,
  `jiaoriqi` date NOT NULL,
  `hege` int(36) NOT NULL,
  `cancipin` int(36) NOT NULL,
  `who` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`gongxu_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `gongzuo`
--

INSERT INTO `gongzuo` (`gongxu_id`, `gongzuoling`, `gongxumingcheng`, `shouriqi`, `jiaoriqi`, `hege`, `cancipin`, `who`) VALUES
(1, 'L15071104-1-0715', '切割', '2015-07-08', '2015-07-23', 10, 5, '');

-- --------------------------------------------------------

--
-- 表的结构 `huoliao`
--

CREATE TABLE IF NOT EXISTS `huoliao` (
  `hl_date` date NOT NULL,
  `tuhao` varchar(255) COLLATE utf8_bin NOT NULL,
  `gongzuoling` varchar(255) COLLATE utf8_bin NOT NULL,
  `mingcheng` varchar(255) COLLATE utf8_bin NOT NULL,
  `shuliang` int(36) NOT NULL,
  `beiliao` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`gongzuoling`),
  UNIQUE KEY `gongzuoling` (`gongzuoling`),
  UNIQUE KEY `gongzuoling_2` (`gongzuoling`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- 转存表中的数据 `huoliao`
--

INSERT INTO `huoliao` (`hl_date`, `tuhao`, `gongzuoling`, `mingcheng`, `shuliang`, `beiliao`) VALUES
('2015-07-11', 'ABCD1234', 'L15071104-1-0715', '玻璃切割', 23, '23456y'),
('2015-12-12', '123jj', 'dfdfd', 'dfdjfdfj', 4, 'fjfg');

-- --------------------------------------------------------

--
-- 表的结构 `kufang`
--

CREATE TABLE IF NOT EXISTS `kufang` (
  `gongzuoling` varchar(255) COLLATE utf8_bin NOT NULL,
  `hegeshu` int(255) NOT NULL,
  `rukushu` int(255) NOT NULL,
  `fengcunshu` int(255) NOT NULL,
  PRIMARY KEY (`gongzuoling`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- 表的结构 `qianzi`
--

CREATE TABLE IF NOT EXISTS `qianzi` (
  `qid` int(11) NOT NULL AUTO_INCREMENT,
  `gongzuoling` varchar(255) COLLATE utf8_bin NOT NULL,
  `zhijian` varchar(255) COLLATE utf8_bin NOT NULL,
  `huitu` varchar(255) COLLATE utf8_bin NOT NULL,
  `jiaotu` varchar(255) COLLATE utf8_bin NOT NULL,
  `zhuguan` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`qid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `role`
--

CREATE TABLE IF NOT EXISTS `role` (
  `role_id` int(16) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=17 ;

--
-- 转存表中的数据 `role`
--

INSERT INTO `role` (`role_id`, `role_name`) VALUES
(1, '内勤'),
(2, '编程员'),
(3, '检验员'),
(4, '技术员'),
(5, '装配工'),
(6, '操作工'),
(7, '辅助工'),
(8, '钳钻工'),
(9, '钳焊工'),
(10, '库管员'),
(11, '总经理'),
(12, '副总经理'),
(16, '副主管'),
(15, '生产主管');

-- --------------------------------------------------------

--
-- 表的结构 `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_bin NOT NULL,
  `password` varchar(255) COLLATE utf8_bin NOT NULL,
  `telephone` varchar(16) COLLATE utf8_bin NOT NULL,
  `number` varchar(40) COLLATE utf8_bin NOT NULL,
  `dept_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=5 ;

--
-- 转存表中的数据 `user`
--

INSERT INTO `user` (`user_id`, `username`, `password`, `telephone`, `number`, `dept_id`, `role_id`) VALUES
(1, '叶涛', '123', '159', '611', 1, 1),
(2, 'asd', '123', '123', '123', 0, 0),
(3, '叶涛', '1234', '13112345678', '123456789012345678', 3, 5),
(4, '7', 'thinkgem', 'admin', '1111111', 0, 4);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
